package Multithreading_1

import org.junit.Test
import kotlin.test.assertEquals

class Test {
    @Test
    fun testOfMultithreading() {
        val result =
            testPerformanceOfMultithreading(1000000, 4567, 56789, 4)
        println("A result for the one thread: ${result[1]/1000000} ms")
        println("A result for the several threads: ${result[3]/1000000} ms")
        println("Coefficient is ${result[1].toDouble() / result[3]}")
    }
}

package Multithreading_1

import java.lang.Math.abs
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.CyclicBarrier
import kotlin.random.Random

fun countArray(array: Collection<Int>): Int {
    var counter = 0
    for(i in array) {
        counter+= abs(i) % 2
    }
    return counter
}

fun testPerformanceOfMultithreading(sizeOfArray:Int, leftRange:Int, rightRange:Int, amountOfExecutorThreads: Int): Array<Long> {
    val rangeThatShouldProcessOneThread = sizeOfArray / amountOfExecutorThreads
    val array = List(sizeOfArray) {Random.nextInt() % 6}
    var counterForExcludedArrayConstruction= leftRange
    val excludedArray = List(rightRange - leftRange + 1) {
        val i = counterForExcludedArrayConstruction
        counterForExcludedArrayConstruction++
        array[i]
    }
    val counterForExcludedArray = countArray(excludedArray)
    val startTimerForOneThread = System.nanoTime()
    val counterForOneThread = countArray(array)
    println()
    val endTimerForOneThread = System.nanoTime() - startTimerForOneThread

    var counterForPreparedArrayConstruction = 0
    val preparedArrays = ArrayDeque<MutableList<Int>>(amountOfExecutorThreads)
    for (i in 0 until amountOfExecutorThreads) {
        preparedArrays.addLast(MutableList(rangeThatShouldProcessOneThread) {
            val j = counterForPreparedArrayConstruction
            counterForPreparedArrayConstruction++
            array[j]})
    }
    val differenceInSizesBetweenArrayAndPreparedArrays = array.size - preparedArrays.first.size * amountOfExecutorThreads
    if(differenceInSizesBetweenArrayAndPreparedArrays > 0) {
        for(i in differenceInSizesBetweenArrayAndPreparedArrays - 1 downTo 0)
            preparedArrays.last += array[array.size - 1 - i]
    }
    val startBarrier = CyclicBarrier(amountOfExecutorThreads + 1)
    val endBarrier = CyclicBarrier(amountOfExecutorThreads + 1)
    val arrayOfResults = CopyOnWriteArrayList<Int>()

    val threads = Array(amountOfExecutorThreads) {Thread(Runnable {
        startBarrier.await()
        val preparedArray = preparedArrays.removeFirst()
        arrayOfResults.add(countArray(preparedArray))
        endBarrier.await()
    })}

    for(i in threads)
        i.start()
    val startTimerForMultithreading = System.nanoTime()
    startBarrier.await()
    endBarrier.await()
    val endTimerForMultithreading = System.nanoTime() - startTimerForMultithreading
    val counterForThreads = {
        var result = 0
        for(i in arrayOfResults)
        result+=i
        result}()
    return arrayOf((counterForOneThread - counterForExcludedArray).toLong(), endTimerForOneThread,
        (counterForThreads - counterForExcludedArray).toLong(), endTimerForMultithreading)
}
